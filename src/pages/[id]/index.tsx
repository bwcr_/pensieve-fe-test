import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { useRouter } from "next/router";
import { Card, CardBody, Heading, Stack, Text } from "@chakra-ui/react";
import { PokemonDataById } from "../../types/pokemon";
import Image from "next/image";
import Layout from "../../components/layout";
import Head from "next/head";

export const getServerSideProps: GetServerSideProps<PokemonDataById> = async (
  context
) => {
  const GRAPHQL_URL = process.env.NEXT_PUBLIC_POKEMON_GRAPHQL_API_URL;
  const id = context.params?.id;
  const res = await fetch(GRAPHQL_URL!, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `
        query samplePokeAPIquery($id: Int!) {
            pokemon_v2_pokemon_by_pk(id: $id) {
            id
            name
            base_experience
            height
            is_default
            pokemon_species_id
            order
            pokemon_v2_pokemonabilities {
                pokemon_v2_ability {
                name
                }
            }
            pokemon_v2_pokemontypes {
                pokemon_v2_type {
                name
                }
            }
            pokemon_v2_pokemonsprites {
                sprites
            }
            }
        }
        `,
      variables: {
        id: id,
      },
    }),
  });
  const data: PokemonDataById = await res.json();
  console.log("🚀 ~ data:", data);
  return {
    props: data,
  };
};
export default function Page({
  data,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const router = useRouter();
  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  const src =
    data.pokemon_v2_pokemon_by_pk.pokemon_v2_pokemonsprites[0].sprites
      .front_default;
  return (
    <div>
      <Head>
        <title>{`Pokemon - ${data.pokemon_v2_pokemon_by_pk.name}`}</title>
        <meta
          name="description"
          content={`Pokemon - ${data.pokemon_v2_pokemon_by_pk.name}`}
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Layout title={data.pokemon_v2_pokemon_by_pk.name}>
        <Card>
          <CardBody>
            <Stack spacing={4}>
              {src && (
                <Image
                  src={src}
                  alt={data.pokemon_v2_pokemon_by_pk.name}
                  width={96}
                  height={96}
                />
              )}
              <Text>
                Base experience: {data.pokemon_v2_pokemon_by_pk.base_experience}
              </Text>
              <Text>Height: {data.pokemon_v2_pokemon_by_pk.height}</Text>
              <Text>
                Is default:{" "}
                {data.pokemon_v2_pokemon_by_pk.is_default ? "Yes" : "No"}
              </Text>
              <Text>
                Species ID: {data.pokemon_v2_pokemon_by_pk.pokemon_species_id}
              </Text>
              <Text>Order: {data.pokemon_v2_pokemon_by_pk.order}</Text>
              <Text>
                Abilities:{" "}
                {data.pokemon_v2_pokemon_by_pk.pokemon_v2_pokemonabilities
                  .map((ability) => ability.pokemon_v2_ability.name)
                  .join(", ")}
              </Text>
              <Text>
                Types:{" "}
                {data.pokemon_v2_pokemon_by_pk.pokemon_v2_pokemontypes
                  .map((type) => type.pokemon_v2_type.name)
                  .join(", ")}
              </Text>
            </Stack>
          </CardBody>
        </Card>
      </Layout>
    </div>
  );
}
