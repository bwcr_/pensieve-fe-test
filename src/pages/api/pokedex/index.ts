import type { NextApiRequest, NextApiResponse } from "next";

// Define your Pokedex data here
const API_URL = process.env.POKEMON_API_URL;
export interface Result {
  name: string;
  url: string;
}

export interface ResponseData {
  count: number;
  next: string;
  previous?: any;
  results: Result[];
}

// GET /api/pokedex
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResponseData | { error: unknown }>
) {
  try {
    const response = await fetch(`${API_URL}/pokemon?limit=10&offset=0`); // Fetch data from the Pokemon API
    const data = await response.json();
    res.status(200).json(data);
  } catch (error) {
    res.status(500).json({ error });
  }
}
