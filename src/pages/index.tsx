import {
  Center,
  Heading,
  FormControl,
  Input,
  InputGroup,
  InputLeftElement,
  Icon,
  Container,
  SimpleGrid,
  GridItem,
  HStack,
  Button,
  Flex,
  Stack,
} from "@chakra-ui/react";
import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import Head from "next/head";
import React, { useEffect } from "react";
import { PokemonData, PokemonTypes } from "../types/pokemon";
import { FormProvider, SubmitHandler, useForm } from "react-hook-form";
import { getStringParams } from "../utils/getStringQueryParams";
import { SearchNormal1 } from "iconsax-react";
import { useRouter } from "next/router";
import { PokedexList, PokedexListFilters } from "../components/pokedex-list";
import Layout from "../components/layout";
interface Response {
  data: PokemonData;
  types: PokemonTypes;
}
export const getServerSideProps: GetServerSideProps<Response> = async (
  context
) => {
  const GRAPHQL_URL = process.env.NEXT_PUBLIC_POKEMON_GRAPHQL_API_URL;
  const search = getStringParams(context.query.search);
  const offset = getStringParams(context.query.offset) || 0;
  const sort = getStringParams(context.query.sort) || "asc";
  const type_id =
    typeof context.query.types === "string"
      ? [context.query.types]
      : context.query.types;
  const limit = 20;
  const typesRes = await fetch(GRAPHQL_URL!, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `
    query samplePokeAPIquery {
      pokemon_v2_type {
      id
      name
      }
    }
    `,
    }),
  });
  const typeData: PokemonTypes = await typesRes.json();

  const res = await fetch(GRAPHQL_URL!, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: `
      query samplePokeAPIquery($search: String, $type_id: [Int!], $sort: order_by, $limit: Int, $offset: Int) {
      pokemon_v2_pokemon(
        where: {
        name: {_ilike: $search},
        pokemon_v2_pokemontypes: {pokemon_v2_type: {id: {_in: $type_id}}}
        },
        order_by: {name: $sort},
        limit: $limit,
        offset: $offset
      ) {
        id
        name
        base_experience
        height
        is_default
        pokemon_species_id
        order
        pokemon_v2_pokemonsprites {
        sprites
        }
        pokemon_v2_pokemontypes {
        id
        pokemon_v2_type {
          name
        }
        }
      }
      }
    `,
      variables: {
        search: search ? search + "%" : "%",
        type_id:
          type_id || typeData.data.pokemon_v2_type.map((type) => type.id),
        sort: sort,
        limit: limit,
        offset: offset,
      },
    }),
  });

  const data: PokemonData = await res.json();
  return {
    props: {
      data: data,
      types: typeData,
    },
  };
};

export default function Page({
  data,
  types,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
  const router = useRouter();
  const searchParams = router.query;
  const defaultValues = {
    search: searchParams.search || "",
    limit: 20,
    offset: searchParams.offset || 0,
    sort: searchParams.sort || "asc",
    types: searchParams.types || [],
  };
  const form = useForm({
    defaultValues,
  });
  useEffect(() => {
    form.reset(defaultValues);
  }, [searchParams]);
  const handleSubmit: SubmitHandler<typeof defaultValues> = (values) => {
    const search = values.search || "";
    router.replace({
      pathname: "/",
      query: { ...router.query, search },
    });
  };
  return (
    <div>
      <Head>
        <title>Pokédex</title>
        <meta name="description" content="Pokédex list page" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <FormProvider {...form}>
        <Layout title="Pokédex">
          <form onSubmit={form.handleSubmit(handleSubmit)}>
            <HStack mb={6}>
              <FormControl>
                <InputGroup>
                  <InputLeftElement pointerEvents="none">
                    <Icon as={SearchNormal1} color="gray.300" boxSize={5} />
                  </InputLeftElement>
                  <Input placeholder="Search" {...form.register("search")} />
                </InputGroup>
              </FormControl>
              <Button type="submit">Search</Button>
              <Button onClick={() => router.replace({ pathname: "/" })}>
                Reset
              </Button>
            </HStack>
          </form>
          <SimpleGrid columns={12} spacing={6}>
            <PokedexListFilters types={types} />
            <Stack as={GridItem} colSpan={{ base: 12, md: 10 }}>
              <PokedexList data={data} />
              {/* Add Pagination using button group */}
              <Flex justify="space-between">
                <Button
                  onClick={() => {
                    const offset =
                      Number(defaultValues.offset) -
                      Number(defaultValues.limit);
                    router.replace({
                      pathname: "/",
                      query: { ...router.query, offset },
                    });
                  }}
                  isDisabled={Number(defaultValues.offset) === 0}
                >
                  Previous
                </Button>
                <Button
                  onClick={() => {
                    const offset =
                      Number(defaultValues.offset) +
                      Number(defaultValues.limit);
                    router.replace({
                      pathname: "/",
                      query: { ...router.query, offset },
                    });
                  }}
                  isDisabled={
                    data.data.pokemon_v2_pokemon.length < defaultValues.limit
                  }
                >
                  Next
                </Button>
              </Flex>
            </Stack>
          </SimpleGrid>
        </Layout>
      </FormProvider>
    </div>
  );
}
