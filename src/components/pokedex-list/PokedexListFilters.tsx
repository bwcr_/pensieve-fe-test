import {
  FormControl,
  GridItem,
  FormLabel,
  Select,
  Stack,
  CheckboxGroup,
  Checkbox,
} from "@chakra-ui/react";
import React from "react";
import { PokemonTypes } from "../../types/pokemon";
import { Controller, useFormContext } from "react-hook-form";
import { useRouter } from "next/router";

export default function PokedexListFilters({ types }: { types: PokemonTypes }) {
  const router = useRouter();
  const form = useFormContext();
  return (
    <Stack mb={6} as={GridItem} colSpan={{ base: 12, md: 2 }}>
      <FormControl id="sort">
        <FormLabel htmlFor="sort">Sort by</FormLabel>
        <Controller
          render={({ field }) => (
            <Select
              {...field}
              onChange={(e) =>
                router.replace({
                  query: { ...router.query, sort: e.target.value },
                })
              }
            >
              <option value="asc">Ascending</option>
              <option value="desc">Descending</option>
            </Select>
          )}
          name="sort"
          control={form.control}
        />
      </FormControl>
      <FormControl id="types">
        <FormLabel>Types</FormLabel>
        <Controller
          render={({ field }) => {
            const value =
              typeof field.value === "string" ? [field.value] : field.value;
            return (
              <CheckboxGroup
                defaultValue={value}
                onChange={(val) =>
                  router.replace({
                    query: {
                      ...router.query,
                      types: val.map((v) => v.toString()),
                    },
                  })
                }
              >
                <Stack direction="column">
                  {types.data.pokemon_v2_type.map((type) => {
                    const isChecked = value.includes(type.id.toString());
                    return (
                      <Checkbox
                        key={type.id}
                        value={type.id.toString()}
                        isChecked={isChecked}
                      >
                        {type.name}
                      </Checkbox>
                    );
                  })}
                </Stack>
              </CheckboxGroup>
            );
          }}
          name="types"
          control={form.control}
        />
      </FormControl>
    </Stack>
  );
}
