import {
  Heading,
  Card,
  CardBody,
  Wrap,
  WrapItem,
  CardFooter,
  Tag,
  TagLabel,
  LinkBox,
  LinkOverlay,
} from "@chakra-ui/react";
import Image from "next/image";
import React from "react";
import NextLink from "next/link";
import { Pokemon } from "../../types/pokemon";

export default function PokedexListItem({ result }: { result: Pokemon }) {
  const front_default =
    result.pokemon_v2_pokemonsprites[0].sprites.other["official-artwork"]
      .front_default;
  return (
    <Card h="full" as={LinkBox} p={0}>
      <CardBody>
        {front_default && (
          <Image src={front_default} alt={result.name} width={96} height={96} />
        )}
        <LinkOverlay as={NextLink} href={`/${result.id}`}>
          <Heading noOfLines={2} as="h2" size="sm" textTransform={"capitalize"}>
            {result.name}
          </Heading>
        </LinkOverlay>
      </CardBody>
      <CardFooter>
        <Wrap>
          {result.pokemon_v2_pokemontypes.map((type) => (
            <WrapItem key={type.pokemon_v2_type.name}>
              <Tag
                textTransform={"capitalize"}
                size="md"
                borderRadius="full"
                variant="solid"
              >
                <TagLabel>{type.pokemon_v2_type.name}</TagLabel>
              </Tag>
            </WrapItem>
          ))}
        </Wrap>
      </CardFooter>
    </Card>
  );
}
