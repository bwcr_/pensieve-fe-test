import { SimpleGrid, GridItem } from "@chakra-ui/react";
import React from "react";
import { PokemonData } from "../../types/pokemon";
import PokedexListItem from "./PokedexListItem";

export default function PokedexList({ data }: { data: PokemonData }) {
  return (
    <SimpleGrid spacing={4} mb={6} columns={{ base: 1, md: 2, lg: 4, xl: 5 }}>
      {data.data.pokemon_v2_pokemon.map((result) => (
        <GridItem key={result.name}>
          <PokedexListItem result={result} />
        </GridItem>
      ))}
    </SimpleGrid>
  );
}
