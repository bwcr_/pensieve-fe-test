import { Center, Container, Heading } from "@chakra-ui/react";

export default function Layout({
  title,
  children,
}: {
  title: string | React.ReactNode;
  children: React.ReactNode;
}) {
  return (
    <Container as="main" py={6} maxW="container.xl">
      <>
        <Center mb={6}>
          <Heading textTransform={"capitalize"} as="h1" size="xl" mt={8} p={4}>
            {title}
          </Heading>
        </Center>
        {children}
      </>
    </Container>
  );
}
