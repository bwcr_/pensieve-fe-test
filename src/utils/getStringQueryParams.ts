export function getStringParams(query: string | string[] | undefined) {
  return Array.isArray(query) ? query[0] : query;
}
