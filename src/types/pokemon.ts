export interface PokemonData {
  data: {
    pokemon_v2_pokemon: Pokemon[];
  };
}
export interface PokemonTypes {
  data: {
    pokemon_v2_type: {
      id: number;
      name: string;
    }[];
  };
}

export interface Pokemon {
  id: number;
  name: string;
  base_experience: number;
  height: number;
  is_default: boolean;
  pokemon_species_id: number;
  order: number;
  pokemon_v2_pokemonsprites: PokemonSprite[];
  pokemon_v2_pokemontypes: PokemonType[];
}

export interface PokemonSprite {
  sprites: {
    other: {
      home: {
        front_shiny: string;
        front_female: string;
        front_default: string;
        front_shiny_female: string;
      };
      showdown: {
        back_shiny: string;
        back_female: string;
        front_shiny: string;
        back_default: string;
        front_female: string;
        front_default: string;
        back_shiny_female: string | null;
        front_shiny_female: string;
      };
      dream_world: {
        front_female: string | null;
        front_default: string;
      };
      "official-artwork": {
        front_shiny: string;
        front_default: string | null;
      };
    };
    versions: {
      "generation-i": {
        yellow: {
          back_gray: string;
          front_gray: string;
          back_default: string;
          front_default: string;
          back_transparent: string;
          front_transparent: string;
        };
        "red-blue": {
          back_gray: string;
          front_gray: string;
          back_default: string;
          front_default: string;
          back_transparent: string;
          front_transparent: string;
        };
      };
      "generation-v": {
        "black-white": {
          animated: {
            back_shiny: string;
            back_female: string;
            front_shiny: string;
            back_default: string;
            front_female: string;
            front_default: string;
            back_shiny_female: string;
            front_shiny_female: string;
          };
          back_shiny: string;
          back_female: string;
          front_shiny: string;
          back_default: string;
          front_female: string;
          front_default: string;
          back_shiny_female: string;
          front_shiny_female: string;
        };
      };
      "generation-ii": {
        gold: {
          back_shiny: string;
          front_shiny: string;
          back_default: string;
          front_default: string;
          front_transparent: string;
        };
        silver: {
          back_shiny: string;
          front_shiny: string;
          back_default: string;
          front_default: string;
          front_transparent: string;
        };
        crystal: {
          back_shiny: string;
          front_shiny: string;
          back_default: string;
          front_default: string;
          back_transparent: string;
          front_transparent: string;
          back_shiny_transparent: string;
          front_shiny_transparent: string;
        };
      };
      "generation-iv": {
        platinum: {
          back_shiny: string;
          back_female: string;
          front_shiny: string;
          back_default: string;
          front_female: string;
          front_default: string;
          back_shiny_female: string;
          front_shiny_female: string;
        };
        "diamond-pearl": {
          back_shiny: string;
          back_female: string;
          front_shiny: string;
          back_default: string;
          front_female: string;
          front_default: string;
          back_shiny_female: string;
          front_shiny_female: string;
        };
        "heartgold-soulsilver": {
          back_shiny: string;
          back_female: string;
          front_shiny: string;
          back_default: string;
          front_female: string;
          front_default: string;
          back_shiny_female: string;
          front_shiny_female: string;
        };
      };
      "generation-vi": {
        "x-y": {
          front_shiny: string;
          front_female: string;
          front_default: string;
          front_shiny_female: string;
        };
        "omegaruby-alphasapphire": {
          front_shiny: string;
          front_female: string;
          front_default: string;
          front_shiny_female: string;
        };
      };
      "generation-iii": {
        emerald: {
          front_shiny: string;
          front_default: string;
        };
        "ruby-sapphire": {
          back_shiny: string;
          front_shiny: string;
          back_default: string;
          front_default: string;
        };
        "firered-leafgreen": {
          back_shiny: string;
          front_shiny: string;
          back_default: string;
          front_default: string;
        };
      };
      "generation-vii": {
        icons: {
          front_female: string | null;
          front_default: string;
        };
        "ultra-sun-ultra-moon": {
          front_shiny: string;
          front_female: string;
          front_default: string;
          front_shiny_female: string;
        };
      };
      "generation-viii": {
        icons: {
          front_female: string;
          front_default: string;
        };
      };
    };
    back_shiny: string;
    back_female: string;
    front_shiny: string;
    back_default: string;
    front_female: string;
    front_default: string;
    back_shiny_female: string;
    front_shiny_female: string;
  };
}

export interface PokemonType {
  id: number;
  pokemon_v2_type: {
    name: string;
  };
}

export interface Pokemon_v2_ability {
  name: string;
}

export interface Pokemon_v2_pokemonability {
  pokemon_v2_ability: Pokemon_v2_ability;
}

export interface Pokemon_v2_type {
  name: string;
}

export interface Pokemon_v2_pokemontype {
  pokemon_v2_type: Pokemon_v2_type;
}

export interface Pokemon_v2_pokemon_by_pk {
  id: number;
  name: string;
  base_experience: number;
  height: number;
  is_default: boolean;
  pokemon_species_id: number;
  order: number;
  pokemon_v2_pokemonabilities: Pokemon_v2_pokemonability[];
  pokemon_v2_pokemontypes: Pokemon_v2_pokemontype[];
  pokemon_v2_pokemonsprites: PokemonSprite[];
}

export interface Data {
  pokemon_v2_pokemon_by_pk: Pokemon_v2_pokemon_by_pk;
}

export interface PokemonDataById {
  data: Data;
}
